import watson_developer_cloud as wdc
from pymongo import MongoClient
from json import dumps

# initialize entities or not
init = False

# connect to mongo database
mongoCreds = {
    "host": "ds157971.mlab.com",
    "port": 57971,
    "database": "pg-demo",
    "username": "developer",
    "password": "pg-demo"
}

mongo = MongoClient(host=mongoCreds["host"], port=mongoCreds["port"])
database = mongo[mongoCreds["database"]]
database.authenticate(name=mongoCreds["username"], password=mongoCreds["password"])
movieCollection = database.Movie

# connect to Watson Conversation
conversation = wdc.ConversationV1(username="ENTER USERNAME",
                                  password="ENTER PASSWORD",
                                  version="2017-04-21")
workspaceId = "ENTER WORKSPACE ID"

# initialize entities
if init:
    movies = movieCollection.find()

    values = list()

    for movie in movies:
        title = movie["title"].encode("utf-8")
        values.append({
            "value": title
        })

    r = conversation.create_entity(workspace_id=workspaceId,
                               entity="movie",
                               description="Entity for denoting movies.",
                               values=values)
    print dumps(r)

# talk to the bot
while True:
    inp = raw_input("\n>>  ")
    messageInput = {
        "text": inp
    }

    responseFound = False

    reply = conversation.message(workspace_id=workspaceId, message_input=messageInput)

    if len(reply["intents"]) > 0:
        bestIntent = reply["intents"][0]["intent"]
        if bestIntent == "movie-info":
            movie = None
            for entity in reply["entities"]:
                if entity["entity"] == "movie":
                    movie = entity["value"].encode("utf-8")
                    break
            if movie is not None:
                print "This is what I know about "+movie+":"
                movieInfo = movieCollection.find_one(
                    {
                        "title": {
                            "$eq": movie
                        }
                    }
                )
                print movieInfo["overview"].encode("utf-8")
                responseFound = True

    if not responseFound:
        print "I don't know how to respond to that."
